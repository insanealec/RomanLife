# Day 7-8

Gaius Plinius Secundus and Gaius Plinius Secundus
Pliny the elder and Pliny the younger

They both survived Vesuvius exploding and were eyewitnesses.
The elder was a ship captain, so he took ships to go save people.
He landed with some people in what he thought was a safe place,
but poisonous gas killed him in the night.
The younger saw the explosion and said it looked like an italian pine tree.
Which looks like a mushroom cloud.

Herculanium was covered in volcanic mud.
Pompeii was covered by ash, then covered by pyroclastic blasts.

**Houses**
1. Domus
	- house of the very rich
	- size of city block
	- for extended family of paterfamilias and slaves
	- very good evidence of these houses, bookwriters lived here
	- would have the outside of the house rented out to retail business
2. Insula
	- means island, but also apartment-house
	- also called tenament building
3. Casa
	- hut, shack
4. Nothing
5. Villa
	- the rural house of the very rich
	- domus in the country, maybe with attached bathhouse


Janua - door, January is the door to the year
Janitor - doorkeeper (yanitour), is a slave
decides if you get into the house or not, may be able to bribe
docere - teach, doctor - teacher

Narrow passage to door - fauces "jaws"

Rooms are like little closets you sleep in.
You do your living in the atrium.
The _tablinum_ is the office where you and your scribes do work.
That's where all your documents for your house go too.
Guard will likely be in the back if the homeowner can afford it, likely will for a domus.

At the janua there was an inscription that said "Hello, Money!"
Money was what based their class system.
While they talked about virtue being more important, money was actually what was important.

Swear by Hercules if you're a Roman. Castor if you're a woman.

Vestibule, Priapus, son of Venus and Mercury (Hermes) with a giant dick.
Priapa is a series of poems about him, comic.
Statues often used as scarecrows.

Lararium - niche for house gods
Always had a Chthonic "earthy" god
usually a bearded snake and also the paterfamilias with a toga over head (sacrificing)
Merc in mercury is same as merchant.

Triclinium - dining room, means reclining room, holds 3 couches
people reclined to eat Cena, dinner, most important meal
lie down to eat on their left elbow, reach out with right hand to eat food
3 people per couch
left handed? too bad, you'll be mocked for being clumsy "laevus"


Parties would have the _arbiter bibendi_ who would decide the mix of water to wine.
They would also pick drinking parties, such as a glass for each letter of the hosts' name.

In the dinner room, there was sometimes an unswept floor mosiac
Made to look like there was garbage on the floor, played prank on guests.

They often ate eggs with an egg spoon, so you could open the egg, scoop out the insides and eat it.
Then you had to smash the eggs because if a wizard came along and wrote your name on it,
then through contagion magic, that egg is part of you, so the witch could control you.

Roman houses weren't supposed to get direct water from the aqueducts, but might anyway if they had money...



By the third century BC they already had Insulas (apartments).
Even tall enough for a cow to fall to it's death from.
This is driven by population pressure, a million people in a very small area.

Working man would probably earn a silver coin in a day's labor.
Work ~300 days a year.
Would probably spend 1/3 to 1/4 in rent.
So about 100 _sisterchi_ (silver coins) a year.

^_Opus caementicum_ - pure cement
stands for a very long time
^_Opus icertum_ - rubble & cement
those mixed together, not as strong, but a lot cheaper
_Opus reticulatum_ - brick & cement woven together
weak as the weaving put weaknesses in the wall
_Opus vittatum_ - brick and cement in bands
even weaker as it wasn't woven, just stacked
^_Opus craticium_ - wattle & daub (sticks & mud)
extremely weak

You would put _stucco_ on top of all of it so you wouldn't really know how good the wall was at that time.
You could also just put straw between the cement and stucco and call it a day.
Eventually there'd be a crack in the wall, and someone would know how bad of a contractor you are.
If the owner were to find that, he'd put plaster on it, paint it, then sell it.
If he was to fix up the house instead, there was law against that to prevent kicking
out the residents to refurbish and rent to wealthier people.
A landowner could aquire a building, add a floor, and make more money.
Then the heir could aquire it, add more floors, and eventually fall from more cracks.
The floors and furniture were made of wood.
Any light and heat inside were provided by fire, if the building would fall, it would catch fire.
Like dominoes apartment after apartment would fall and burn.
2 things scare city dwellers the most: the cry of fire, and the crack of a wall.


### Names

Gaius Julius Caesar Octavian
Gaius = praenomen
Julius = nomen gentile -> clan name
Caesar = cognomen -> name within the families within a clan
Octavian = name after getting adopted, comes from previous family

GJCO is so well known for bloodshed he picked a new name,
which was the old one plus Augustus.
A famous Centurion was missing from a party, so the army was going to rebel because they
thought he was murdered. But he just showed up late.

Apicius - famous gormet of the Roman world
Not all of his writings were actually his own.
Look into this for end-of-semester project.
Doesn't have proportions or time, figure it out.

# Day 9

Continued from **Roman6**.

### Sewage

> Cacator, Cave Malum!
"Crapper, beware harm!"

Can't crap in public or in a house, so you crap at the _Latrina_.
Which has benches with holes in it, they were placed along the sewer system.
In most Roman cities, there was a sewer system.
They'd bring in water via aqueduct from streams in the hills,
it'd fill up a fountain, which would then spill if enough wasn't taken,
it'd spill into the streets, which would run downhill.
This would remove horse shit and also what people would dump
from their bedpans from the street.
It would then run into a pit under the ground, which runs under the streets,
then will run into the nearby river.
The volcanic rock at pompeii and herculanum was too _tuff/tufo_ 
so they couldn't build sewer systems.

> Water engineering is Roman civilization. Maybe more than anything else.

Romans had high standards of cleanliness, so there was a small ditch with water
for them to clean themselves.
In the water were sponges on sticks that you'd use to clean yourself,
then you'd put it back for the next guy.
There may have been 2 separate halves to some latrina,
we don't know if men and women or freed and slaves.

_Seneca_ is a great poet, we know a lot (some lies) because of him.
_Lucien_ was his descendent who was also a great poet.
He would read _Nero_'s poetry whenever he'd poop because it sucked and lessened the noise.

Some private houses could have their own Latrina, especially if on a sewer line.

_Cloaca Maxima_ - biggest sewer

_Fullonica_ - laundry
Launderers (fullers) used ammonia (pee) as bleach.
People were supposed to come up and urinate in them,
the fuller or their servant would come out and take it,
then use it inside for bleach.

Nero is an idiot with no self knowledge, who is inept.
In _68 AD_ there's a rebellion against him, Nero ran away,
the senate declared him a public enemy, he eventually commits suicide.
As he dies he says:
> Quialis artifer pereo
"What an arist I am dying here."

In _69 AD_ there were 4 emperors.
First _Galba_ (the first guy to rebel against Nero),
then _Otho_ (friend of Nero's, let him sleep with his wife),
then Roman general in Germany named _Vutullius_
he charges his army into Rome to take over,
then Otho kills himself.
Then _Vestation_ and his sons take over because he hates that dude.

_Consessor_ - Accent Vestation has.
Some accents are considered dumber people than others.
He would pronounce caupo as copo.

Vestation had the idea to tax Fullonica because people were giving 
free pee and the Fullers were profiting. His advisers said it was a bad idea.
He took out a coin, held it under their noses, and said "non olet" (it doesn't stink).

Beginning with the dictatorship of Caesar, you had to bring in goods during the night,
it was illegal to use wheeled vehicles during the day.

### Snack stand

_Thermopolium_ - fast food store
Something to eat around the middle of the day while you're away from home.
Could provide lentils, olive paste, cheese, wine...
Some had places to sit, and maybe even tables. Some were like high class restaurants.
Had round holes in the counter to store vats of food.


### Baths

After walking around, grabbing some food, you may want to take a nap.
You may be able to find a corner in a bath to sleep in.
Then you'd want to bathe after feeling oily.
Baths were a very public affair, everyone bathed around each other.

_Apodyterium_ - clothing offering
Where you take your clothes off before bathing.

_Frigidarium_ - cold bath

_Tepidarium_ - warmer bath, exercise area

_Calidarium_ - hot bath, really hot
Had a scraper for cleaning yourself, you couldn't get your back.
You'd ask someone to get your bath and you'd get theirs.

Cheaper times of day, in the morning before the baths were hotter and at night when they were dirty.
Some bath houses were cheaper than others.

### Grafiti

Could be political or just messages.
There were some that were advertisements for beast fights.


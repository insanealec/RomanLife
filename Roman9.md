# 2/23/17

_serf_
from _servus_ "slave"

_granfalloon_
Imaginary but proud association of individuals.
People are proud to belong or not belong to *thing*.
eg. race, nation, ...

_servare_
perserve
slaves can be eaten, ransomed, soled, or **preserved** to work


Ways to become slave:

1. POW
	- Most common worldwide is to take slaves from war
	- Generals have benefit to go to war, they can collect slaves
	- not a valuable slave
		- economically valuable because of number of them
		- not individually, because not raised as slave, and knew they could escape
		- prone to run away
2. Child of a slave mother
	- always a slave because descendant from a slave
	- common way to become a slave
	- more valuable than POW because they've never been free
		- less prone to escape
		- used to it, can put up what they're used to
-------------
_Less common_
3. Kidnapped
	- illegal
	- done by pirates a lot
	- "like buying off ebay"/"fell off a truck"
	- you know when buying it's probably stolen, but it's cheap...
	- only free again if...
		- can get away
		- get to Roman representative
		- can prove Roman citizen
		- gotta find someone that can prove it
4. Sold by parents
	- wow, that's cold
	- earlier roman era
	- invalid unless _trans tiberim_
		- "across" "tiber"
		- had to sell to someone in Etruria
		- later interpreted to be foreign
	- eventually most of known world is roman
	- so then illegal
5. Debt bondage
	- takes on loan that you can't pay
	- human can be seized as slave
	- one of rights from Struggle of the Orders is plebs make this illegal
	- like 2 before this can still happen if they can get away with it
	- If you can get your freedom
		- you can sue the rich person that did it
		- another rich person that hates them will represent you
---------
6. **Show biz**
	- can be sold or POW like others
	- some people sold themselves to be gladiators
		- contracts show they surrender their citizen rights
		- they list rights in these contracts
			- right not to be beaten
			- right not to be burned
			- right not to be killed
	- only thing you get in return:
		- you have legal right after contract to claim freedom
	- can't prevent owner from sending you into fights that'll likely kill you
		- so then they don't have to pay you amount in contract
	- _infamous_
		- prostitute, gladiator, charioteer
		- can never be Roman Citizen, even if were beforehand
		- too famous to be citizen
		- "_in_ negative _famous_reputation"
		- can be freed, but can't be citizen


If you're an Alba Longan in rome, you get certain rights. You can sign contracts and marry someone.
It's like a foreigner status because of EU. You have rights because you're a part of a federation.
These rights **do not** go to non-citizens that are infamous.

_civitas Romana_ full Roman citizenship
_civitas Latina_ roman citizenship, marry and sign binding contracts

_manus_ - hand; absolute legal authority; also band of armed men
_manumission_ - freeing a slave

Benefits sometimes to have a slave become full citizen, because then they can go into army.

If the slave wasn't useful, they may just be killed.
There was a guy that would feed slaves to lampreys alive.
He went to do this when Augustus was there, but he had a problem with it, so he filled the pool in with concrete.

Cato suggested you work a slave until he's old, then manumet them when they became useless, so they had to feed themselves.

Nutrix, paedagogus, and janitor were likely to be freed in the will of the owner if they did a good job.
Intimate with household. Not obligation, but often done.
Janitor may also be able to buy themselves freedom.
_peculium_ the money a janitor gets through bribes, it actually belongs to his owner
He could invest the money for a while and make even more.
He could offer some money to his owner to buy himself, but his owner could just take it and tell him to get back to work.
If intense enough, he could just kill his owner.
If the parties that did it aren't caught, all of the slaves are crucified.
Owners could allow their slave to have some of the money they get, because it treats them right, and they won't rebel then. Then they can bathe and engage in economics.

Manumission encourages hope within the slaves, so they'll keep working to try and be or feel free.


If you free some slaves some of the time, it gives the others hope they may be free if they reach goals.
If those goals are economic activity, then that benefits you.
It also gives you safety, because people with no hope do dangerous things.
The hope of manumission makes the owner's life easier.

A slave could use their peculium to buy another slave.

Slaves could be treated well so the owner can have emotion from them and the slave could be a boyfriend/girlfriend. They would be sexual favorites and may be manumetted.
When freed, they could be Verna or cinnaedus.
In order to pretend to be younger they may shave and whatnot.

Senators couldn't buy and sell retail, it was looked down on.
But they could rent out retail, and sometimes rent it to their slaves or ex slaves.
The senators have to be super wealthy, but aren't allowed to do some econommic activities.
Income from land is looked on at the best.
If you have a slave that is good at making money, you'd free them and set them up in a shop so you could make more.

_pileius/Phyrgian cap_
in greece = someone from western Asioa (Phrygia) aka Troy
in Rome = liberty
"freedom cap"
_Satenalia_ (festival in december)
eat, drink, and give presents
just like Christmas
You could also play pranks like april fools
Slaves wear a liberty cap and could be free and equal to their masters for a short while.
They also wear it on the day they are freed.


_lictor_
his ceremonial weapon is the _fascis_ a bundle of sticks with an axe head protuding from it.
The more sticks, the more senior the lictor.
Acts as a guard for the consul and praetor.
Sort of acts as a police officer too.
Soldiers aren't allowed in the city during peace time, to keep city free.
They act as a public witness for some documents, like manumission.

For full roman citizenship to a freed slave, the owner needs signatures from their neighbors.

During manumission, the _missio_ (sending) is beaten one last time, then is taken by the _manu_ (hand) and let go to be free.
The lictor then gives the feed slave _nubertos/nuberta_ a ticket which is a title to him/her self.
This proves they are free, in case their ex owner tries to take them back as a slave (illegal).

**753 BC** April 21st
Founding of Rome (traditional date, not necessarily true)

**509 BC**
Last king exiled
By dude named Brutus, not the Caesar brutus, a diff guy.
_Beginning of roman republic_

**264-146 BC**
Punic wars
3 Wars with Carthage
1. Who's gunna run Sicily?
	- Romans
2. Who's gunna be dominant power in Mediterranean?
	- big one
	- couple decades later
	- hannibal brought elephants over alps
		- decimated armies
		- killed consuls
	- he lost, Romans won
		- empire and army lost
		- one small empire left
3. Is carthage allowed to continue to exist?
	- 3 years
	- Carthage is one city state
	- Romans were afraid
		- paraniod about cry "hannibal at the gates"
		- wipe them off the map

Before first punic war was conflict of the orders.


After this the Romans called the Mediterranean "our lake"
If you're rich, you could import wines from Greek
	- risk of ship being taken by pirates
	- risk of ship sinking
The rich could become even richer.

More slaves also brought in, as more slave labor comes in, less work for honest folk to do.
Crisis of economic inequality after the punic wars.
Tremendous opportunity for many, desperation for others.
Farmers losing land and property, more congregate into Rome.
They are citizens without land that live in the city, all they do is go to political assemblies.

Political parties arise:

_Populares_
Senators interested into promoting rights and interests of common people.

_Optimates_
The commoners should just shut up, everything will be fine.


**390 BC** 
Gauls defeated Rome, survivors had to pay the to go away.
Used counterweights to weigh tribute.
Someone noticed the gold was more than the weight.
General waved sword around, put it on the counterweight.
Said _vae victis_ woe to the defeated.




#Roman Life Day 1
Similar historical societies with similar historical pressues will yeild similar historical results.
_Or something_


#Day 2

**Horatius Cocles**
> guy with one eye

**753 B.C.E. April 21st**
Founding of Rome.

Betwen these dates Rome is ruled by Kings.

**509 BCE**
Roman Republic begins
Democracy also begins in Athens.

Did Rome reshape it's history to be based on the Greeks they idolized?

**31 bce**
Battle of Actium

Augustus/Octavian wins, end of the republic, beginning of empire.

**476 CE**
Abdication of Romulus Augustus
End of the empire in the west.
Did he exist? Name after the founder and first emperor?

Empire lived on in the Eastern Roman Empire.

**1453 CE**
Sack of Constantinople
Became Istanbul. Same place still.
Ottoman empire took over, death of eastern roman empire.


Rome was built between some Greek cities at the south of Italy,
and the Etruscans at the north. It's on a river on the Tiber River.

So in order for people to go between those cultural centers,
they had to go through Rome.

Etruscans had control of Rome when it was a monarchy, but when the 
king was overthrown, they sent armies to take it back.
Lars Porsenna was the lead of that Etruscan army.
Horatius Cocles and some friends were standing gaurd on the bridge,
his friends ran to the other side and cut the bridge, while he held off the army.
He then dove into the river in full armor and swam across to the other said.
Obviously a myth, but he became a hero.


Mettus Curtius (sometimes Marcus, but that's wrong)
Chasm opened up in Roman Forum, which is the public center.
Possible because it was a drained swamp, like ohio.
Soothsayer said chasm wouldn't go away, but would get larger unless
the most important thing in the city was thrown in.
Mettus had no question while other people were talking about it,
he immediately rode into the chasm, which then shut behind him.
The most important thing was giving up himself for his country.

Cincinnatus (means curly)
Roman senate could appoint a dictator for 6 months or duration of emergency,
whatever is less. They had complete control during that time, but had to law down
emperium at that time, and should have before then. Don't give anyone this power
unless you trust them.
One of the consuls was killed and another was kidnapped, senators were sent 
to fetch him, his wife delayed them until he put on his toga (he was plowing the field).
Then he raised an army and saved the consul and fought a war in two weeks.
At the end of that time he laid down his power since his job was done, and went back to farming.
Since he was the archetype of the minute man, Cincinnati was named what it was.

Rome was at war with Alba Longa, a latin city and this was during the Monarchy.
The next king that took over wasn't as militant. He suggested they settle this through a competition.
Rome had a set of triplets, the Horatius triplets, Alba Longa had triplets (Curatius).
Rome suggested they had a combat of champions between the triples, winner take all.
Whatever set wins, that city state wins and the other submit to be assimilated by the winner.
They take their battle oaths, then go to battle.
Curatius 3 was wounded severely by Horatus 3, but H3 was killed.
C2 was wounded slightly by H2, who was then killed by C2.
H1 then had to fight all 3, so he ran away, but for a battle reason.
Since the others were wounded and chasing after him, they would run after and be separated by each other,
so he then killed them one by one since he was able to separate them.
Everyone in Rome was so happy for Horatius. Except for one lady, his sister Horatia,
since she was engaged to one of the Curatius.
So then he killed her, and he was charged with the terrible crime, the death penalty, the charge
was waging war against the state for treason.
Horatius' father asked to have the case made in front of an assembly of all Roman citizens.
He then said he had 3 sons and a daughter, and now he only has 1 son, and he saved Rome, so to let him go.
They did, he was freed, charges were dropped.
This story tells us you can't be killed without a trial if you're a citizen, and you have the right to an
appeal, even if you are condemed. Also if you're sad, you should wipe your tears away and be happy
for the state. The point is also that we tell stories about things that we fear,
the hidden message is that you can't trust anyone, especially if they're your brother.

One of the kings of Alba Longa, Numitor, had a wicked brother named Amulius.
He took over the throne and became king. Numitor had a daughter named Rhea Sylvia.
She could get married and have sons, who would then be threatening to Amulius' throne.
He didn't kill her, because of Miasma. But he made her become a sort of a nun for Vestia.
After 30 years of service to the goddess, so they have no fertile time anymore,
as women are chosen before they become a virgin to serve this goddess.
They tend the fire, which is the soul of the city.
Amulius noticed she was somehow pregnant, she claimed it was Mars, the god of war,
who empregnated her. Since it was the rules, she had to be buried alive.
But after her sons were born, who were then sent down the Tiber river in a basket.
They end up at the base of the **Capitol Hill**. There was a she-wolf on the top of the hill,
whose cubs had died, so her teats were painful from being full of milk.
She heard babies crying, so the two boys got her milk and lived through the night. (clearly myth)
The next day they are found by Faustus (name means lucky), and the kids are raised by him and his wife,
Lupa (means she-wolf), and the boys are named Romulus and Remus.
Romulus and Remus like to go up the river to Alba Longa and steal sheep from rich people.
Remus gets caught and dragged up in front of Amulius, who says he's bad for trying to steal Numitor's sheep.
So he's given to Numitor as a slave. Numitor's a cool dude, and they become friends.
Eventually they figure out that they're related. They all 3 get together, kill Amulius, and put Numitor in charge.
Then Romulus and Remus want to found a new, big city in the hills they grew up, big enough to hold
people's sheep and keep everyone safe in war. They fought about the name of calling it after themselves.
To make the decision, they go watch birds (who carry messages from the gods, called augury) on separate hills.
They saw eagles, birds of Jupiter, so they figure who saw the most would win.
Reme said he saw six, the Romulus was like, oh, I saw 12, twice as many, so I win. Romulus is a skeeze.
Romulus starts building a mile long wall by himself, Remus mocks him and says the wall was low enough to jump over.
So Romulus kills him. The foundations are stained by fraternal murder.
Story tells us that no matter how much power they have, they will turn it against each other back home.
Romulus is a rusticus (rustic) hero. When ruling the world, don't be rustic, be urbanus (urban).

Gaius Julius Caesar was urbanus, and he was a jackass that could get away with anything.
As a young man he has to leave rome for political reasons and travels around, but he
gets kidnapped by pirates in the eastern mediteranean. This story is history, not myth.
If you get captured, if you're rich enough you can ransom yourself, otherwise you get sold as a slave.
Since Caesar was important the pirate suggests they ransom him for 2000 coins. Julius gets furius,
says he's too important for that, suggests he gets ransomed for 5000 coins.
During his time with them, while sending messengers to get money, he writes songs for them.
Sometimes they like him, sometimes they hate them, so he says he'll crucify them all.
Once the money is raised, he goes home, raises an army, and hunts down and crucifies them all.
He does cut their throats because he liked them.
And during his assassination his last words were actually _Kai ov tekvov_.
Which "means you too, child?" He did sleep with Brutus' mom... But he wasn't the father.
But he was reminding Brutus of that, that he screwed his mom.
# 3/23/2017

### Roman Army

History can be split into 2 periods.
1. Before Marius - _Before 100 BCE_
	- militia summoned for military emergencies
	- disbanded after emergency
	- not a standing army of proffessional soldiers
	- to be eligible you have to be:
		- adult male
		- Roman Citizen with civitas Romana
		- over 14
		- younger than 47 _juvenis_ (young man/"to help")
		- who owns significant property
			- small farm
			- or more
			- because have stake in way things are now, won't revolt
2. After Marius - _After 100 BCE_
	- Professional standing army
		- No longer minuteman approach
		- Professional fighting force
	- Eligible:
		- adult male roman citizen
		- Don't have property requirement
		- enlist for career
			- 20 to 25 years
	- paid living wage while in army
	- on retirement
		- get chunk of land
		- get chunk of cash

_Gaius Marius_
7 Consulships -> 5 sequential
Unheard of, but he did it, was allowed to do it, and succeeded.
He innovates the army to ensure Rome could be secure.

**Veii**
"10 year" (probably not) long war against etruscan city that Romans conquered.
Probably many years, go back home and get more soldiers, so on...
Gauls invade and sack Rome during this time.
"Second founder of rome" convinces them not to give up.

**Quintus Fabius Maximus**
"the 5th" - only called by mother, quintus was a common name since it was a number
Second name - _nomen gentile_ tribe name
Fabius -> faba -> "bean"
Asked when he was gunna go fight hannibal, says never.
He just follows Hannibal around, when he turns around to fight, they retreat.
He knows Hannibal is far away from supply chain and is losing men, materials, ...
gets to be called the "delayer"

_PUblius Cornelius Scipio_
defeated hannibal in africa
calls descendants Africanus

Slowly farmers are getting poorer and poorer because of wars and having to give farms to bigger farm owners.


**Populares/Optimates**
Upper class political parties.

Social stability is lost so...:

P: say we need to look at needs of common people to stop bleeding

O: best when we're in charge, so common people should STFU


Cash is Gaius' (P) idea to try to reconstitute the class of small farmers,
but the soldiers don't know how to run a farm,
they've been soldiers. And they didn't invest their money or anything.

But army is not mortal. In old army, after each war all knowledge and skill was lost.
With new army, the skilled soldiers can train the new soldiers.

_commititiones_
_quirites_
_milites_

New system makes soldiers loyal to their generals, not to their country.

_Publius Cornelius Sulla_ 88 BCE
Charged with fighting war against _Mithrodantes_.
Lots of wars and assassination attempts on this guy.
Comes into Rome with army, hugely illegal.
Gets in trouble for this.
Then finishes war, marches army in again,
then declares himself dictator for life. _perpetuus dictator_
Soldiers are okay with this, so no one can stop him.
He gives himself the power of _proscription_.
_proscription_: name is written out and can be killed with impunity
He then puts a bunch of Populares senators on that list to be killed,
so he can get more power and their property for him and his minions.
He thinks this decisively ends struggle between populares and optimates
and put optimates in charge.
Once he dies, almost all legal innovations he introduced is thrown out,
because people hate him; doesn't matter if it was good or not.



### Armor

_galea_ helmet
_lorica segmenta_ segmented body armor; banded armor
less restrictive than plate, cheaper and easier to fix than scale
_centurion's helmet_ take orders from this guy; horse hair crest on top
_gladius_ Roman sword; called spanish sword sometime;
original roman sword was blunt on the tip;
point deadlier than the edge;
discovered point is deadlier in spain during war, by _Scipio_
Romans tend to have cultural conservatisim, not changing unless have to,
but they will when it comes to better conquering and militaryness;
short sword with counterweight is easier to hang onto and control
_hasta_ older type of legionary spear;
for stabbing or throwing;
breakable adjustment where staff meets head of spear, intentional
_pilum_ (pila) plural; long shaft, long neck for small, sharp metal head
strictly for throwing; long shaft bends after thrown, can't be used against you;
any time a missle is thrown at enemy, it could be re-used by them (arrows);
can be re-used after battle won, can be hammered back into place, but not immediately reusable
_cornu_ horn used for battlefield direction to communicate changes to plans
_tuba_ trumpet, same thing, used for different sound than horn
_caligae_ hobnailed boot for legionary soldier
_scutum_ (shield) large enough to protect all vital organs
edges are beveled so you can hook together with other guys on the line
_testudo_ (tortoise formation) lock and hold shields above heads
protects you and the people around from arrows
one guy can't form one, but a division can

_aquila_ legionary eagle; religious devotion to eagle
if eagles lost to enemy, they must get the eagles back
_aquilifer_ eagle-bearer
_signum_ (manus) another signal for the army
_signumfer_ bearer of signum
_vexillum_ legionary flag
_vexillarius_ flag-bearer

_ballista_ (a thing that throws things - greek) catapult
there could be specialist engineers that made this kind of thing for army on battlefield
greatest engineer of ancient world was Archimedes; helped defend sicily against rome
_castra_ Roman military camp
you can still see footprint of these all over today
if caster or chester is name of town in englad, probably used to be a castra


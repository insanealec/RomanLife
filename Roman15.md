# 4/13/2017

### Women

Hard to get information about women because the books were written largely by men,
for an audience, concieved to be primarily men.

_Publius Clodius Pulcher_
patrician by birth, adopted by younger plebeian so he can become tribute
also seduced Caesar's wife
Institues free food giveaway in 58 BC, after annexation of Cyprus
Because of this, the government will feed poor Romans below certain income rate.
Passed by _plebiscitum_, plebian legislation.
Most popular populares.

_Titus Annius Milo_
anti-publius
known not for clever, but for strength
opposite because he's plebian by birth, but in the optimates party
Was pushed out of election for consulship because of riots that Publius started,
which caused the elections to keep being pushed off that whole year.

Entourage of everyone involves thugs to "bodyguard" the politician (ex/current gladiators).
Fight breaks out between these 2 politicians and their thugs.
Publius is injured and flees, while the other party follows.
Titus tells his men to go into the building he was hiding in and kill him, so Publius died.
Figures he's safe even though he's committed murder, because optimates believe
they can kill any populares, as long as it helps Rome. Seems to be true.
Then Publius' wife holds a public funeral, where he's naked so the crowd can see his wounds.
The crowd figures they'll get their revenge outside the law, they riot and burn down senate house.
Senate declare _Pompei the great_ as emergency consul (illegal, but they do anyway).
Pompei holds trial of Titus, and even though he had the best lawyer, he gets exiled.
All thanks to _Fulvia_, his wife.

**44 BC** _Gaius Julius Ceasar_ was murdered at the foot of the statue of Pompei the great.
His assassins shout liberty and wave their knives shouting liberty.
Thinking they're not going to be prosecuted because they killed the tyrant.
Then there's a public funeral for Caesar, so they can count the wounds.
Then there's a riot, and the killers have to flee the revolution.
_Marcus Antonius_ is not entirely responsible for this idea, he gets it from his wife,
_Fulvia_. She's a great politician and she had done that before.

Roman women do not have the same equality as Roman men, 
but they aren't property, they have rights.

_Manus_ has power to legally beat, and sometimes kill, someone.
_cum manu_ is where a bride goes into her husband's family with all her stuff, she goes into his manus.
_sine manu_ bride goes in with stuff, but it's all her stuff still, in _patre potestas_ of old family
no one in groom's family has manus over her, bride can take all her stuff in divorce

Lucretia good woman - suffered but got rapist killed
Tarpia bad woman - let sabines into city, got crushed by their shields
Clodia bad woman - had affair
Cornellia good woman - raised the Gracci, wrote a book
Octavia - sister of Augustus, married to Antony
Livia Augustus - married to Augustus, mother of heir

Scribonia = Augustus = Livia = Drusus Claudius
     |                        |
    Julia           Tiberius     Drusus = Antonia (Octavia's daughter)
     				(sicko)				|
     						Germanicus		Claudius (physically disabled, very smart, stuttered)

Marcus Vipsanius Agrippa considered candidate, good general.
Was going to have Marcellus, Octavia's son be the heir, but he died.
He's married to Juliua, so that power can pass to him, then to his children.
Gaius, Lucius, Postumus, Agrippina (daughter)
Augustus didn't want to leave power to Tiberius because he was a pedophile.
But, Agrippa died, so Augustus adopts his children.
Then they all die at some point, but Postumus is exiled.

No other choice. Tiberius and Julia marry so Tiberius becomes heir.
She then sleeps with everyone else because they're so unhappy in their marriage.
Augustus then exiles her because he's trying to pass marriage laws.
So then Agrippina gets married to Germanicus, and they're cool together.
Augustus forces Tiberius to adopt Germanicus, so then both sides of family are Julian again.
Then Augustus dies and there's riots because Tiberius isn't the son of a god.
Germanicus puts down rebellions, is a son of a god, and people love him. Then he dies suddenly.

Livia counts as good because all the crimes (arranging death) she committed was for the
good of her son.
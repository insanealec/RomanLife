#Day 5

###3 classes of people wear pants
1. women
2. barbarians (gauls)
3. Roman soldiers on campaign in the north (alps, where it's cold)


Since women can divorce whenever, they have so much power to inflict monetary pain
on her husbands family.
When they talk about Roman decadence/decline, they're talking about women's rights.

A lot of shows show Romans as sexually depraved, they have different liberties and sexual customs.


**Sexual Options For Male Citizens**
- Can have sex with wife
	- very important
	- so important that Augustus passed legislation to promote "legal" roman offspring and marriages
		- criminalize adultery
		- gives awards to those who had 3+ children in their marriage
- can have sex with slaves
	- taking advantage of another person is a question of morality
	- from a slave-owners view, this is a good option
	- built into a slave system so slaves are sexually available to their owners
	- social value is that next generation of slaves is related to free blood of household
		- believed to create bond of loyalty between slaves and free population in household
		- offspring of slave-women are always slaves, so they don't have rights
		- called a _verna_ (masculine or feminine)
			- born, raised, and living in same household
			- valuable because rules they live in, they've always lived in
			- used to "dealing with it"
			- viewed as loyal
		- owners think they know what's going on, we unfortunately don't have slaves' point of view
	- permissible to have sex with both male and female slaves
- can have sex with prostitutes
	- legal
	- can sometimes be viewed as praiseworthy
	- 3 classes of Roman female prostitute
		1. _scortum_ (low class)
			- sex for money
			- "streetwalker" may work in a bordello though
			- neuter noun, only one referring to a human in Latin
				- engaged in sex so frequently, she has become "leathery"
			- shame attaches to prostitute, not client
			- "erotic art" in Pompeii wasn't art, it was a menu for illiterates
			- bordellos had sex rooms that were the size and design of bathroom stalls
		2. _meretrix_ (med class)
			- social and sexual relations for money
			- "girlfriend experience"
			- hired to pretend to be girlfriend
			- keeps going on as the money keeps flowing
		3. _psuedo-amica_
			- gray line between meretrix and amica
			- to prevent this, don't give gifts to your girlfriend
	- classes of male prostitute (allowed if man is dominant)
		1. _cinaedus_ submissive sex (for money or not)
- _amica_ "girlfriend"
	- sex but not a prostitute
	- voluntary social/sexual relations with a man
	- you gotta be good to do this
	- you have to be able to persuade a citizen class woman to be your amica
	- sometimes she doesn't like you anymore, so you get a gift, then she likes you again
	- this means he doesn't have a girlfriend, he has a meretrix, or a pseudo-amica


Same-sex relations both _were_ and _were not_ tolerated in the ancient world.
Depending on who was doing what to whom.
What is important, is if you're behaving in the appropriate manner.
If a male citizen if having sex with a male slave, the citizen has to be the top, they have to be dominant.
_Cinnedus_ is the passive partner in a same-sex relationship between men. (comes from greek, means different thing)
Never permissible for a citizen class male to be the passive partner in a relationship.
Those who were passive were shamed by being called cinnedus.
Citizen male to citizen male was okay, as long as you were the dominant one.
Otherwise keep it secret.

Unless you're Julius Caesar, rules don't apply to that bastard.
When he was young and traveling (before pirates) he had a passive relationship with an older man,
a king of some place, so the older guy was the dominant.
This was okay in Greece, but since he was Roman it wasn't okay.
He had no consequences and just laughs whenever someone brings it up.
His soldiers say he's "every man's woman, and every woman's man".


_Marcus Procius Cato_
often called Cato the Censor
The Censor's job is to run a census.
He also evaluates the respectability of individual Roman citizen, 
and determines if they're allowed in the senate, their class, or even in the citizenry.
He gave a guy a hard time because he kissed his wife in the daytime while his daughter was in the room.
Cato had a huge issue with this.
Cato had only kissed his wife once in the daytime, because she was scared of a thunderstorm.
Other guy said "Don't worry Jupiter will make it thunder again soon."
Sees a friend of his come out of a bordello, Cato tells him good job.
He says this, because if someone is with a prostitute, then he isn't seducing citizen class women
away from their husbands.


Love in marriage was very likely. Epitaphs written by spouses have been very tender.
If couples stay together, it's because they want to. Especially when divorce was a common thing.


Clodia is having an affair with Catullus, her husband Melellus Color looks the other
way because her brother Clodius was important.
Catullus writes her very good love poetry, even one about getting a blizzard of kisses from her.
He calls her Lesbia in the poems because he pirated some of them from a previous poet.
Because of his poetry, he's called _mollis_ which means soft.
Men want to be tough, _durus_.
He then gets shamed because he's engaging in relations in a manner perceived to be soft.
Not because of the affair and whatnot (which was illegal).
He then writes a poem to his friends, proving he is _durus_.
> I will fuck you in the ass,
I will fuck you in the mouth,
Forgot this line
I will fuck you north and south.



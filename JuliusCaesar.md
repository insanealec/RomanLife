# Julius Caesar 1953

Opens with some jackass asking some gambling dudes what trade they are.
One of them tells a joke that he's a cobbler and a "mender of bad soles"
Jackass asks them why they aren't working, funny dude says to rejoice Caesar's triumph
Some other dude in a white toga joins the Jackass in berating the carpenters.
They both say not to worship caesar and to not hang his visage everywhere.
Then they get arrested or something and are called traitors.
Marullus and Flavius

Caesar tells Antonius to touch Calpurnia while he races so she stops being sterile.
Then a soothsayer tells him to beware the ides of march.
Caesar's like, "this dude dreamin' let's go"

Brutus seems kind of sad and tells Cassius he's not agile enough to do sports.
Cassius then berates him and tells him no one likes him and that Brutus is too blind to see the hidden meaning in his own words.
Brutus thinks the people want Caesar to be king, and he doesn't want that, neither does Cassius.
Cassius tells a story of how he and Caesar swam in bad waters, but Caesar was being a bitch and asked for Cassius' help so he didn't sink. Then Cassius angrily said "And this man is now become a god."
He then keeps bringing up how Caesar can't be a god because he gets sick like mortals and whines like a little girl.
Brutus doesn't have an answer for Cassius yet, but he says he'll think about it.

Caesar tells Antonius that he's wary of Cassius because he looks thin and hungry, that makes him dangerous.
Antonius says he's not dangerous, but Caesar says he wouldn't be if he was fatter (more content).
He says he's really dangerous because he's smart and always wants more.

Casca then goes to speak with Cassius and Brutus.
He says that the shouting was about people offering Caesar the crown, and he waved it away 3 times, but less each time.
Antony offered him the crown, which was actually like a flower crown or something.
People hollered so much, it filled bad smell in the air and Caesar fainted and foamed at the mount, Casca had to try not to laugh.
Caesar wrote it off and was like, "sorry guys, I'm sick, mah b".
Casca told them that Maurelus and Flavious were pulling scarves off Caesar's statues.
Cassius offers Casca dinner, but he has plans, so they'll have dinner the next night.
Cassius tells Brutus that Casca's rudeness helps people absorb his wit in conversation.
As they set off from each other, Cassius tells Brutus to think of the world.

Cassius talks to himself about his plan to make Brutus lose his nobleness for his plans.
He's gunna throw notes into Brutus' windows like they're from citizens and they're about how he's viewed by them.
Then it theatrically storms.

Casca's running around with a knife. Cicero finds him and is like "why you out this late, bro".
Casca's afraid of how bad it's storming. He says he's seen some crazy stuff like a dude's hand on fire.
Cassius then comes across him and startles him.
He then compares Caesar to this bad night, to try and sway Casca to his side.
Casca agrees that something needs to be done, so there is no tyranny.
Cinna comes out of nowhere and is like, did ya get Brutus in our club yet?
Cassius then gives Cinna some notes to place in different places for Brutus to find.
Cinna goes and tells Cassius that Catellus is waiting for him at his house.

Brutus is talking to himself and yells at Lucius to wake him up, but he's really asleep.
He wakes up and gets Brutus his taper.
Brutus says that the only way to fix the problem with Caesar is by his death.
Crowning him is like a sunny day that brings out the adder, you gotta watch where you step.
Even if he starts good, it's in his nature to be bad, so you gotta kill him before he hatches.
Lucius brings him one of the papers and Brutus tells him to check if the ides of march are tomorrow.
The letter makes him fill in the gaps in the letter with how he must do what's good for rome.
Cassius comes with some other dudes and introduces them.
Brutus said that they need to have an oath so that it's sure they're all together.
Cassius offers to bring Cicero into their gang, everyone agrees, Brutus says he wouldn't do it, everyone agrees.
Theseus asks if anyone else needs to die, Cassius says Antony should die too, but Brutus disagrees as it will be too bloody of a path to cut off the head and hack off the limbs. "Let us be sacrificers, but not butchers Caius.""
Then they set out in the morning saying to not look suspicious.

Brutus' wife (Portia) talks to him and is like "what's the matter honey", but he won't answer.
She keeps pushing asking if he's sick or what's up. But she says he must be sick in the mind, and she needs to know.
She keeps pushing even more trying to guilt him into telling her.
She says Cato's her dad and Brutus is her husband, so she's stronger than most women and won't tell anyone his secret.
Someone then knocks on the door, so he tells her to go inside.
It was Caius Ligarius that came to the door.
He's kind of sick, but willing to put that aside to help Brutus.
Then they leave to chat.

Caesar's wife is having a nightmare where he gets murdered. He hears and has someone go tell the priests to make a sacrifice and to come back to tell him if they are successful.
His wife tells him he shouldn't go and he says nothing scares him and if anyone were to try something they'd run away after looking at how serious his face is.
She tells him that there have been bad omens about, but he says they're to the world, not at him.
She says the omens have to be about him, because omens don't happen to homeless people.
He says death happens when it happens, and it's no use hiding in fear.
His messenger comes back saying the priests say for him to not go out, they couldn't find a heart in their sacrifice.
Caesar says he's gunna go out because if he stays in, then he's a beast without a heart, and to prove his power he will go forth, no matter the risk.
His wife asks him to do it for her, so he says that's fine.
Theseus comes to say hey, then Caesar says to tell the senate he won't be there today, but not to lie because he's not sick. Caesar doesn't lie. Theseus asks why, so he can tell him.
"The cause is in my will, I will not come, that is enough to satisfy the senate."
But he tells Theseus the real reason 'cause they're bros, and it's because his wife (Calpurnia) wants him to stay home because she had a dream that his statue had a hundred spouts of blood that Romans smiled and bathed their hands in.
Theseus says she's mistaken and that the vision actually means that Rome will suck reviving blood from him.
Theseus says he knows this is the meaning because the senate will crown him today, but they're minds will change if he doesn't come.
Caesar says Calpurnia looks pretty dumb now and that she needs to give him his robe so he can go.
A bunch of people come over to Caesar's for a drink now. They all hang and chat.

Some dude with a cool beard has a letter for Caesar to watch out and names all the conspirators in the letter.
He waits on the stairs so he can read it to Caesar in order to try and save him.
He yells at Caesar to read his schedule, but Cassius diverts.
Popilius says that he hopes their purpose thrives, so Cassius and Brutus worry that the secret is out.
Trebonius draws Mark Antony out of the way.
Metellus Simba asks Caesar to un-banish his brother, but Caesar's like "hell naw".
Brutus then kneels before Caesar too asking that Publius Simba's banishment be repealed.
Cassius asks too, but Caesar says he's as constant as the Northern Star.
While Caesar's going on about how constant he is, Casca stabs him first.
Then everyone else stabs him too, while Brutus backs away holding his knife.
Caesar goes to him for help, but Brutus stabs him.
"Et tu Brute? Then fall Caesar."
One of the conspirators says "liberty, freedom, tyranny dead!"
Brutus then yells for everyone to stand still and ambition's death is paid.
They let Publius go and tell him that he won't get hurt.
They say they saved Caesar from fearing death by killing him young.
Brutus says they should bathe their hands in his blood. Cause he's weird I guess?
Marcus Antony sends a messenger to kneel and say Brutus is great and smart and he looks up to him and that he feared and loved Caesar. He wants to know if he can safely come to Brutus and ask why he killed Caesar. He says he won't love Caesar as much dead as Brutus living, and will follow Brutus through this time.
Brutus tells him that Antony is a wise Roman and for him to come over and he'll be satisfied and unharmed.
Brutus says "Welcome Mark Antony" when he comes, but he ignores him and just walks over to Caesar's body.
He talks to Caesar asking if all his triumphs are shrunk to this small measure, and wishes farewell.
Antony says he doesn't know what they're up to to kill such a noble dude as Caesar, but if they wish him harm to kill him now right after Caesar as that would be an honor for him to die beside Caesar.
Brutus says they must appear cruel, but their hearts are pitiful and he'll explain himself after talking to the crowd.
Antony then shakes their hands. Brutus, Caius Cassius, Theseus Brutus, Metellus, Cinna, Casca, and Trebonius.
"Gentlement all."
Antony says he still loves them, but they are enemies of caesar and he needs an explanation now.
Brutus said that their intentions were noble and if Antony was Caesar's son he'd be cool with it.
Brutus also agreed that Antony could talk and Caesar's funeral, but only if he said good things about the conspirators. Cassius told him not to do so and to be wary because Antony could sway the crowd.

Once the guys left Antony went over to Caesar's body and apologized for being nice to them.
Antony says that he prophesizes a civil war and destruction to come to the conspirators.

As people are shouting about Caesar, Brutus is telling them that he killed Caesar "not because I loved Caesar less, but because I loved Rome more."
He asks them if they'd rather Caesar be dead and live as free men and they all say no.
He then challenges them to speak up if he's offended them, while also saying any who speak up are vile, terrible people that hate their country.
They all fall in and say "none Brutus, none"
While he's telling them about how Caesar deserved it for his ambition, Antony walks out with the body as people shriek.
"As I slew Caesar for the good of Rome, I have the same dagger for myself when it shall please my country to need my death."
People yell, live Brutus.
Brutus leaves to allow Mark Antony to speak and all the crowd is wary of Antony because they don't want him to speak bad of Brutus because Caesar was a tyrant and they are lucky to be rid of him.
The crowd keeps getting louder, to get their attention Antony says "friends, romans, countryment, lend me your ears"
Antony has a speech about how Caesar was ambitious, but lists all the great things Caesar did and after each one says "but Brutus says Caesar was ambitious, and Brutus is an honorable man"
Then Antony pauses to allow the crowd to make connections between his speech and Caesar not actually being ambitious and actually wanting what's best for rome.
Antony mentions Caesar's will, but says he can't read it so to not make the people mad about how much Caesar loved them.
But the crowd persists, so Antony steps down to Caesar's corpse and shows the crowd where each person stabbed Caesar and explained how they betrayed him and how great Caesar fell.
He then reveals the corpse so that the romans will hate the traitors.
He stirs more mutiny within them and as they go asks them to wait so he can read the will to them.
Apparently in the will Caesar gives every person 75 monies and his personal orchards.
Then people riot in the streets.

"The conspirators having fled, there came to Rome the young Octavius whome Caesar had adopted as his son. So did he and Antony divide the power between themselves and prepare to make war against Brutus and Cassius for the empire of the Romans."

Brutus and Cassius were fighting because Cassius sold some of his land and Brutus told everyone he was a lame-o.
He also denied Brutus gold for his army which is causing some fighting.
He then said he'd give him the money, so they're friends again and apologized.
Brutus was sad because Portia died, she killed herself in his absence.
There was news Cicero died.

Brutus fell asleep and saw Caesar's ghost who said they'd meet again at Phillipi.

Brutus and Cassius bid each othe farewell, as they were unsure if they'd survive the coming battle.
It's also Cassius' birthday and he pledges to fight for liberty.

Mark Antony's army lie in wait for ambushing Cassius' and the entire time there was just the constant beat of drums. It was very suspenseful.
The swords looked very fake and floppy, as though they were cardboard covered in foil.
As projectiles were flying they just looked really floppy too.	
Antony raised his sword, everyone prepared their arrows, and when he lowered his sword they all fired.
Antony's army won, they had the surprise attack and were more prepared.

Cassius and a couple others were able to flee, so he sent someone to check if the nearby army was friend or foe after they ran.
The guy he sent off got chased down and killed.
Cassius said he should die on his birthday, so he makes Cirra a free man and tell him to stab him with the sword used to stab Caesar. Which he does.
It's kind of funny watching Cassius die as he's very clearly holding the sword off to his side like I did when playing swords as a kid.

Next we're at the broken down campground of Brutus' after he fled. One of his friends that was left behind tells Octavius that he is safe.

Then we see Brutus and the rest of his beaten up army, he tells them to raise their heads and rest.
Then he looks over a rock and sees Cassius dead behind it.
He tells Phillumnius that he needs to run on his sword, it's better to jump in the pit, then to tarry and be pushed into it.
He gets someone else to hold his sword while he runs himself into it. (strato?)
As he dies he asks Caesar's soul to be still.

Octavius and Antony go into the tent where Brutus' body is kept and Antony says he is the most noble of the conspirators because he's the only one that did it for the good of rome, not out of envy.


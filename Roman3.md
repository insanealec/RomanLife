#Day 4

Familia
 the people and property under the control of paterfamilias

paterfamilias
 the oldest living make ancestor in the paternal lie

patria potestas
the legal power of the paterfamilias over his descendants and their property

in potestate
the term describing someone who is under someone else's patria potestas

manus
another term for patria potestas most ofen used with reference to legally sanctioned violence

suscipium
the picking-up ritual in which a child is brought under the patria potestas of the paterfamilias.. or isn't

peculium
property that legally belongs to the paterfamilias but which is allowed to his children (or slaves)



The paterfamilias can have his slaves or children killed.
Exercised rarely, but did happen from time to time.
Was used to help keep order in the country.
You were supposed to summon a family council, and have a trial to determine
if your son is eligible for the death penalty. Has to be treason or planned murder of the father.

If you killed your father, the penalty was to be put in a sack with a snake, a monkey, and a tiger,
then they beat the sack with a stick and threw you in the Tiber.

In the reign of Augustus, there was a guy named Tricho (means hairy).
He was an equis (second class) and he killed his son.
He was then attacked by all sons and fathers present at the forum,
with a pointed stick/pen/stylus thing that was used to write on wax,
that had a flat blunt end to erase (and beat people).
Augustus had to dispatch guards to protect him.
This was the last instance of father killing son.

Infant exposure was a thing, where they would just throw out babies,
usually in the Sewer.
Children were brought before the paterfamilias, who picked it up (became part of the family),
or it was taken a way and thrown away somewhere.
Once brought into the family, they couldn't be thrown away, they were under protection.

If a paterfamilias had a daughter, who married then had a son, that son
would not be a part of the family, but they would be in the father's patria potestas.


Until emperor Rome, women wouldn't have names. They would just be named by numbers instead of names.
(quintus, septimus, sextus, all numbers) same happened to sons.
They just named them this as children possibly, because they would die someday, so why get invested?
Name fades out early for men, but continues on for women into the early empire.
EG dude named Appius Claudius Pulcher
had 2 sons Ap. C. P. Jr, and Publius Clodius
3 daughers Clodia Prima, Clodia Secunda, Coldia Tertia

2 kinds of marriage, one brings the bride under the patria potestas, one doesn't.
Married with Manus (legal violence) or not?
cum manu: older style of marriage, brought the bride under the patria potestas for her husband
This is the old patricius style marriage.

Marriage comes first, as consequence you have love after that.
Bride price, man wants to marry woman, pays her family.
Also a Dowry (key to status of Roman women),
big sum of money that comes with bride into her husband's family.
Husband's household will have new mouths to feed, so bride brings money
to have less costs that come into the family.

Paterfamilias' can converse and sell their daughters to each other, discussing the terms.
Marriage has to be past the daughter's puberty.
Late teens for Roman men.
Roman marriages only occur if there is consent between all parties, so the women do have that right to refuse.
Whereas in Germanic areas during that time, there was marriage by abduction.
Someone could kidnap the lady, impregnate her, then go back and they would have to be married.
In Rome, if that was tried, the kidnapper would be killed for violence,
and there would be no stain against the kidnapped. The child would be exposed.
The paterfamilias could make the daughter's life hell if she doesn't agree, but she isn't forced to marry.


sine manu: later (only plebian, but later standardized) bride stays under patria potestas of her family
When paterfamilias dies, the sons become their own paterfamilias.
The wife may have a tutor appointed to her who would represent her in court if she needs it.
She is likely treated as _sui juris_, she can do things of her own right.
Increasing customary practice for mature women to be sui juris, especially if they own property.
Customary practice is practically law.

In sine manu, when the woman gets married, she stays in the patria potestas of her father.
The husband can't beat her, because he doesn't have manus on her. If he does, he'll get in trouble.
Since women live in a house where the people in it don't have physical power over her, it's somewhat liberating.

In cu manu, the wife may not get a divorce. The husband probably will not get a divorce.
You have to go to a council of older relatives and get permission to divorce, even if paterfamilias.
If you don't have kids, then adopt one, that's fine.
If you kick out your wife then you may be banished next census.

In sine manu, still only valid when all parties consent.
If a party withdraws consent in this system, a bride or a groom can dissolve marriage at any time.
Patres familiarum are not allowed to direct children to get divorce.
One will say to the other in front of witnesses "I am divorcing you."
Or sends a letter signed by witnesses.
Then the divorce is done, so the wife goes back to her father's house or sui juris.

The money that came with wife (dowry) has to go out with her (since it's still hers).
A consequence is that a husband can't give a gift to his wife legally in case of divorce.
Another is that the wife can exact a huge economic penalty on that family if she gets pissed.
A huge blow no matter what social status you are at.
So the husband may not talk about politics or bite his tongue if he thinks she's cheating.


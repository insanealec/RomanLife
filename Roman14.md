# 4/11/2017

#Augustus

_concordia ordinum_
Harmony of the orders.

**Syme, Roman Revolution**

_Marcus Antonius_ - Caesar's right hand man
In terms of ability, successar to Caesar.

_Gaius Julius Caesar Octavianus_ - Augustus
His birth name was Octavianus, the rest is his adopted name from Caesar
He is the _son of god_ which was taken pretty seriously, especially by soldiers.
Adopted in Caesar's will.

_Marcus Lepidus_ - Takes over calendar (pontifex maximus) after Caesar dies
Couldn't count to 4, put leap day in every 3 years,
screwed up calendar for 12 years after he died.

These 3 made up the 2nd Triumvirate.

Assassins of Julius Caesar flee to the eastern mediterannean to try to get more money and soldiers.
They are then defeated at phillipi.
Then the new triumvirate splits up the Roman Empire to suit themselves.
One day Octavian goes into Lepidus' land, says he's in charge, and the soldiers back
him up because he is the son of god. So all of Lepidus' bases belong to him.
Octavian kills a lot of people, but not Lepidus, he lets him be _Pontifex Maximus_ still.
But no one cares about him, he gets called on last at the senate.
Octavian is a smart, clever asshole.

Mark Antony and _Cleopatra_ figures that with the resources of the east and Egypt,
and Antonius' battle skills, they figure they can take rome away from Octavian.
**31 BC - Battle of Actium**
Sea fought battle off of western greece.
Octavian beseiges the city of Alexandria and takes it, but not before
Antony and Cleopatra kill themselves in early 30 BC so they aren't taken in.

Caesar and Cleopatra had a son called _Caesarion_ (little Caesar).
Not legal son, but actual son of Caesar.
Only one left from Caesar's bloodline, was child during seige of Alexandria.
He "went missing" after the seige, likely killed for being the actual/physical son of god.


Octavian has to create a monarchy in order to save Rome, but if he does,
he'll be killed just like Caesar was.
So he re-brands himself as _Augustus_, he "won't" kill you.
Says he's not king or dictator, says he's the _princeps Senatus_.
Makes it so people don't vote the "senate" is more efficient.
His opinion is taken first in debates, so he gives them,
and everyone is free to give their opinion in agreement.
If someone doesn't agree, they'll find themselves agreeing.
For years he has himself or his friend elected consul, then there are riots,
so instead he "suggests" the senate give him _potestas consularia_ (consuls' power).
He has the consul's power forever, no matter who is consul.
Senate and citizenry need reformed? Need census of all of rome?
He also gets _potestas censoria_ (censor's power).
Maybe on "temporary emergency basis" he also gets _potestas tribunicia_,
can veto anything. Someone hurting someone? Veto. Some law trying to pass? Veto.
When Lepidus dies, he also accepts _pontifex maximus_ and controls religion.

Now he:
- controls law
- controls the army
- controls who is a citizen
- controls veto-ing
- controls religion

He is a monarch, but no one calls him out on it.
As long as he doesn't say or act like a monarch, everything is fine for him.
Everyone is tired of civil war.
Also, everyone is tired of his real power.
_imperator_ - victorious soldiers can vote this to their commander
Augustus rules the soldiers as this and also as _filius divi_.
"on temporary emergency basis" he gets rule over all domain
that takes soldiers to run, he rules all lands as proconsul (except sicily and small places,
which use the same old type of roman rule).
He puts people in power places to rule for him, but he can take it away whenever he wants.
This system is powerful, but it takes someone with 2 qualities.
1. son of god
2. political genius
Never comes together again.

# 2/21/17

_alma mater/nutrix_
"nursing mother"/wet-nurse
Slave that recently gave birth that will feed and raise the recent child of the family.

Quintilian, Institutio Oratoriae (elements of oratory)
This dude says nutrix is necessary, but not suffcicient.
If they speak weird latin, then your child will have to re-learn latin.
So your nutrix should speak good latin and maybe greek.
Also need a _paedagogus_ or "child-herder"
to protect your child so they don't get kidnapped by slavers.

Eventually there's formal education, first to supplement, then to replace the apprenticeship system.

1. Litterator
	- ABC
	- tor - guy who does something
	- a "letter"-or
		- he "letters" you
		- you learn to read and write in latin
		- and do simple sums
	- kids (both boys and girls) go
		- if their parents pay for them
		- from 5/6 years old until they understand what they're being taught
	- math is done with a counting box
		- kind of like an abacus
		- don't use roman numerals for math
	- you're done when you're done and got it
	- paid employee of the parents (end of every quarter)
	- just like nowadays, no one like teachers
	- while _vis_ violence against a roman is illegal, it's allowed for
		- soldiers who are beaten for discipline
		- children who need discipline in school
			- corporal punishment for these children was:
				- **a cane filled with lead**
	- he can give treats as a reward
	- it was known that beating was ineffective for teaching for a long time before this
2. Grammaticus
	- teaches to read and write in greek
	- further reading of latin literature
	- may study geometry, astronomy, etc
	- you can use this knowledge to make yourself more liked culturally
		- could be used to sway the jury because you're smarter
		- the power of oratory
		- know what time it is from the shadow
	- boys only for this teacher
		- women could be educated more, but by tutors at home, not in public
	- went to grammaticus until they were legally adult
		- when they "put on their manly toga" 
			- _toga virilis/pura_ 
			- plain white, no decoration
			- separates child from his childhood and childish garments
		- eligible for military service
		- eligible for work
		- _14 years old_
3. Rhetor
	- "flow-er"
	- make your speech flow
	- like grade school in public speaking
	- men only
	- intensive public speaking


People don't learn philosophy to learn the truth, they use it to be able to speak persuasively.
Rhetor teaches how to be persuasive in speaking.

The point of Rhetor assignments is to create a persuasive argument whether it's true or not.
_Suasoria_ "persuasion" ficticious persusasive speech. "Persuade hannibal not to cross the alps with his elephants."
_Controversia_ "controversy" ficticious legal case. You have this case presented to you, defend the side you are assigned.

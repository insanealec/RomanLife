# 3/16/17

### Important dates

**753 BCE April 1st**
Rome Founded

Between here were kings. Last one was Tarquinius Supertous

Myth: Collatinus says his lucretia is the best while others are bragging about their wives (while out to war).
His cousin, son of the king, Sextus Tarquinius (archeological evidence he existed, story may not be true),
goes back to Rome, to his cousin's house while he's not there, then rapes her and laughs at her.
He goes back to the battlefield, smiles every time Collatinus says something.
Then they and Brutus (not the Caesar one) are over for dinner where Lucretia tells everyone what happens,
says they should do something, then kills herself. They then take her body outside and rebellion sparks.
Brutus and Collatinus are elected first consuls since the're sick of kings.
Likely not historically accurate, but may have included real people.

**509 BCE**
Republic founded

**31 BCE**
Battle of Autium
Battle between Marc Antony and Octavian.
Octavian beats Antony and Cleopatra.
Roman republic over, one person in charge.
Augustus has to pretend that he's not a monarch or a dictator, because Caesar didn't pretend and he was killed.
Augustus "reforms" the republic to be like a monarchy but without looking like it.

**69 CE**
Year of 4 emperors.
Long year of civil war.
Sparked by suicide of Nero, last remnant of Augustus' empire.
Death of first part of empire.

**476 CE**
Romulus Augustulus abdicates.
Last _western_ roman emperor.
Suspiciously named after first king and first emperor.
Could be made up like original romulus.
Civilization was in collapse as people were being murdered and libraries destroyed by germans.


### Government

The kings were the:
- chief legislature
- chief judge
- chief general
- chief priest

Senate
- group of older guys (senex means old man in latin)
- job was to advise king


For republic:
They carve kingship into 3 separate functions.
- Legislature
- judge and general
- priest

legislature is roman people
_populus romanus_
legal adult roman males make up assembly that create laws
they are sovreign
elect the consuls and pontifex maximus

judge/general
_consul/consul_
given to 2 people at a time, both are equal
if they can't agree, nothing gets done
nothing should be done unless everyone can agree

priesthood
_pontifex maximus_
holds office for life

Senate continues to do same thing.
Advises consuls instead of monarch.
Anyone who holds office for a year and maintains monetary requirements becomes senator for life.

Then it becomes the SPQR
_Senatus Populus que Romanus_
senate and people of rome
oligarchic senate and democratic people working together


**Comitia Centuriata**
assembly that meets by centuries
A century is a group of roman male adult citizens who can provide 100 soldiers in time of war.
Citizen soldiers are more loyal and reliable than paid, because they will die for their country.
- can pass laws
- elect public officials
	- censors
	- consuls
		- 2 elected anually
		- 1 year term
		- have imperium
	- praetor
		- some elected anually

Election day announced well in advanced every day leading up about who's up for voting and what issues.
You vote within your century, majority vote wins the vote.
Each century counts as one vote. Majority of citizens determine century, and majority of century wins.
Guys who are richest vote first. Once majority is found, they stop voting.
Democratic, but not too democratic. Kind of like electoral college.
So that it doesn't end up like Athens where instable laws are passed day by day.
First and second winners both become consul.
If people are voting for you, you'd come in a shiny, bright white toga so people could see you.
_toga candidata_ means bright white, we get "candidate" from that term

Every 5 years 2 censors are elected for an 18 month terms.
Only elected to run a census.
and assign men to centuries.

**Comitia Tributa**
"assembly that meets by tribes"
A citizen is assigned to a tribe when he becomes a Roman citizen.
Designed to give ex-slaves less influence on votes.
Elect _quaestor_ (investigator/auditor) 5 per year
- They make sure money goes where it should
- _cursus tionorum_
- "course of offices"
elect _curule aediles_
- like the pleb aediles
- may be patricians
- get a curule chair to sit on

**Concilium Plebis**
"council of plebeians"
with the conflict of orders they got the right to elect their own people
elect _tribunes_ (tribunus plebis)
- not the military officer of same name
- originally 2 elected every year, but raise number to 10
- patricians may not hold or vote for this office
- tribunes are _sacrosanct_
	- illegal to assault them
	- blasphemous to assault them
	- a consul with imperium can have someone arrested
	- if a tribune stands in the way, the lictors can't get through them to arrest the person
- have _veto_
	- "I forbid"
	- can stop any state action, including actions of imperium
- purpose of this power is to protect plebeians from patricians
	- since imperium was originally only patricians, even if it wasn't anymore
	- helps as a check against the dictator-like power of imperium
can pass a _plebiscitum_ which is a law over all citizens and counts as a _lex_
elect _aediles_ (annually)
1. maintain public buildings
	- _aedes_ means temple
	- ROME = AMOR
		- rome was founded by descendant of Venus
		- can't have anything fall into disrepair
	- they pay to keep up buildings
		- gotta be rich to be able to finance reconstruction
		- then they can put their name on it (directly on a plaque atop the door or something)
		- name recognition is a powerful instrument of politics
2. maintain public order in marketplace
	- gambling is illegal, especially dice; causes public disorder; don't do it in public marketplace
	- also set prices of goods
		- if cheese was too expensive that day
		- could force someone to lower prices
		- makes popular among cheese eaters (not producers)
	- rough power to use, have to incur more favor than hatred
3. oversee public entertainment
	- get stipend from state to produce shows
	- better they do, the more memorable their work is, gunna spend own money too
	- becomes more and more expensive; law of diminishing returns
	- some animals were hunted to extinction by romans for beast fights

**contio**
emergency town meeting
summoned by an office official with imperium
rarely used
"I man, I vote"
can pass _lex/leges_ legally binding laws


**Senate:**
body of ex-officials
capped at a few hundred people
comitia centuriata was hundreds of thousands
so senate could actually have a conversation and discuss if law should pass
_princeps senatus_
- senator whose opinion is taken first in debates
_consultum senatus_
"decree of the senate" not law
probably then brought by the consul who suggested it to the comitia centuriata
becomes law or it doesn't

_mos maiorum_
"custom of the ancestors"
unwritten law
Most laws were written down in public
if you could convince people that some law you broke was for use as a custom,
you could be aquitted of the crime


#Day 3

###Free Or Slave?

Freeborn:
ingenuus/engenua
Born free

Freed (ex-slave):
libertus
liberta
libertinus
libertina
Freed slave.

Slavery wasn't based on how someone looked, so a freed slave looked exactly like a normal person.
So the discrimination wasn't there unless you were marked as a slave.


There's also a distinction between _Roman Citizen_ and _non-citizen_.

Eg: Jesus could be executed by the governor, where Paul had to be taken back to Rome.

**Roman Citizen**
**A**
Can have _civitas Romana_
"Full Roman Citizenship" - best kind

**B**
_civitas Latina_
"2nd class Roman citizenship"
Like when you're from a conquered and assimilated country.
i.e. can legally marry people with civitas Romana
can form binding contracts with people who have civitas Romana


The normal, default status of an ex-slave is status B.
They could be status A.
And before they were freed they could be status C.

**A**
If not C, your owner can free you as civitas Romana.
Takes some extra work on their part.
You can do everything, except hold office, because your parents weren't legally married.

**B**
Freed as civitas Latina.
This is the default status of a freed-slave that was a Roman Citizen.

**C**
(infamus) - infamous
Such a bad reputation that even when freed, you could not be a Roman Citizen.
Even if you were taken as a slave in a war, you could not be a Roman Citizen,
since your loyalty lies with a different country.
Also if you were a Prostitue, Gladiator, Charioteer, Actor in many cases.


###Class distinction

**Patrician or Plebeian**

Patrician:
> A descendant through paternal line of the original group of roman senators.

Plebian:
> not

Issue with class, patricians realized they need to be on the good side of plebs,
so they gave them all the same rights.

Nobiles:
> Ruling class
rich and socially influential
patricians and plebeians

Populus:
> everyone else


Spartula:
> basket of food; payment in money
from a patron to client

Patrons and clients
one finds out about another and stuff.
Can't directly tell someone to kill another person,
but could break a chain of responsibility if the client was found beating up someone,
but wasn't told explicitely by the patron.

Patron to patron
amicus to amicus
# 4/18/2017

### Games and shows

- stays during empire, still politically motivated
- very religious

**ludi**
- "public religious festivals"
- public holiday
	- as in no public business occurs
	- _dies fasti_
		- too happy to work on that day
		- celebrating someone
		- festival of apollo
		- emperors birthday
		- private businesses may close too
	- _dies nefasti_
		- too unlucky to do business
- sacrifices at temple
	- gives animal to priest (may need to pay priest)
	- _holocaust_ when priest goes without pay
- free food giveaway
	- cooked right after sacrifice and dispensed
- free entertainment
	- also free chariot races


Chariot racing is more popular than gladiators

**Auriga**
charioteer that directs the team
belongs to group of 4 factions that control monopoly that control races in rome
Greens, blues, reds, whites. Greens and blues absorb the others as minor leagues.

Horses could be famous, just like the riders.
People had to hold onto reigns or they'd fall and they'd get dragged to death.

There are female gladiators.
When it looks like someone is flipping people off or leaning back, that is a symbol of surrender.
_ad ludum_ gladiator for 5 years
_ad gladium_ gladiator till death
_bestiarri_ beastfighter, typically bears, not lions as much
The trainers would sometimes have enrichment with the animals.


### Greco Roman Religion
1. public
2. communal
3. contractual
4. traditional
5. ritualistic
6. animistic _animus_
7. ancestor-worship

haruspex plural haruspices singular
priest dudes that help you get in connection with the gods

sarcophagus - "flesh eater"
pomerium "sacred border of the city"

lemuria "ghost - lemur"

#### Features of Mystery Religion
1. private
2. closed membership
3. rite of cleansing 
	- how to become member
	- babtiso ("dipping" into a fluid, blood or water)
4. mysteries (secret rights)
5. involve a sacred meal
6. where the god (a dying god)
	- dies and comes back to life again
	- persephone, dionyssus
7. is eaten and/or drunk
8. henothestic/monotheistic
	- henotheism in between poly and monotheism
	- heno means one
	- but believes in multiplicity of gods, have a favorite
9. personal relationship with god
	- you ate them
10. they can offer you life after death worth living because they came back from underworld
	- "heaven" german term
11. moral guidance
	- follow rules of community
12. enforced by excommunication
	- if you break rules, you get expelled from community

**Clearly this is _Mithraism_!**
or:
- _Isis cult_
- _Elevsinian mysteries_
- _Bacchanalia_
- _Christianity_

A group of mystery religions arise in roman empire because public religion isn't enough.

Elevsinian is really old.
Follow Persephone/Kore
Daughter of Zeus and Demeter.
Her uncle Hades can't get dates so he drags her down to underworld.
Zeus could get her back if she hadn't eaten anything in the underworld.
Hades tricked her into eating some pomegranite seeds.
So there's a different deal where she can go back to overworld sometimes.
When above there's life, when she goes below there's death.
Like farming seasons and weather changes.
So in Elevsinian mysteries she's eaten in bread.
So if the followers eat that, they get life after death worth living.

Mithraism comes around in persion religion.
Eventually name was mithra, but romans changed to Mithras.
Son of god Ahura Mazda.

Bacchanalia is suppresed by senate.
Elevsinian mysteries is old and respected, but only in Elevsinia, so you gotta go there.
Isis cult is weird to Romans because Egyption gods have bird heads. romans took advantage of these guys
Mithraism was respected, they build caves called Mithraia.
Christianity is viewed weird because they allow criminals in and anyone even slaves and women can be forgiven!

But this is the religion that displaces the rest.
At first they were hated and used as scapegoats.
Once they dropped need to circumsize it was easy to become christian, just had to be circumsized.
Romans hated Christians because they were a group that tried hard to keep recruiting people (missionary).
Was illegal because of Nero.
Lots of persecutions whenever a fire happened.
How did it succeed?
Unpopular with upper class because it allowed lower class in.
Didn't agree with public religion.
Wins anyway.
1. organizational
	- christian republic
	- decentralized
	- but connected widely
	- _urbanus_ citified, associated with christianity
	- _paganus_ rural, associated with polytheism
2. allows women
	- christians can be raised by the moms
	- Mithraism only happens when you become an adult
	- told from beginning that they were special
	- demographic grows over time


_Constantine I_ (along with co-emperor)
edict of milan 313 CE
Christianity becomes legal now.
Before then about 1/3 of Romans are christian
then grows more after emperor support.


### Why did Roman Empire fall.
Not Barbarians because they got trampled so many times by Rome.
Vespacian, when he took over, made a change in military policy.
Which explains why it lasted longer in the East than West.
One of first thing Augustus does is cut army in half because every independant army is a threat.
They do all get their money for retirement.
Vespacian increases Hadrians' wall so that more troops can be anchored on border of empire.
So they they aren't inclined to march into Rome since they have a home at border.
Eventually troops are led back into city again.
Increases economic weight of military. Taxpayers have to pay.
Emperors have to spend more and more money to placate soldiers so they can stay in power.
Military discipline becomes more lax.
Western Empire has too little money because of excessive taxation and loses.
Eastern is more profitable so they can afford to stand against enemies.



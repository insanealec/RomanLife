_factum (non bonum)_

# 4/4/2017

**Cyruus the Great**
First of the Persion emperors.
Creates the biggest empire of the ancient world, stretches from India to Egypt.

_empire_ one nation directly governs another
_hegemony_ one nation has preeminent leadership status over other nations

### Roman Provincial government

1. Before first Punic War _264 BCE_
	- seize _some_ local land
		- land of people who died during war (see war with Veii)
		- _ager pubicus_ owned by the state
			- can be rented by wealthy Romans
			- title still owned by state
		- land Tiberius wanted to distribute to citizens
			- prospect optimates and popules are interested in
	- plant a colony of Roman citizens (imported and local)
		- if you're a lower class citizen, then move to colony to improve your status!
		- some locals are allowed (or forced) to become citizens
			- co opt people into thinking themselves of Romans
			- they can run for office and work with helping Rome for their own gain
			- also get people eligible for army
	- taxes are paid directly to Rome
	- local laws and customs remain in force
		- "you can be in charge under us, or stop being in charge" - if oligarchy
		- if they want to have democracy, let 'em
	- disputes with Roman law and local law get settled in Rome by an official with imperium

2. After Punic War I _246 BCE_
	- seize some local land (as above)
	- import Roman citizens (as above)
	- local laws remain (as above)
	- disputes settled by official with imperium
		- _proconsul/propraetor_
		- appointed by senate
			- disrupts balance of roman constitution
			- tilt away from democracy towards oligarchy
			- has effect of killing republic
	- just like last system, proves to be very effective government
	- taxes for provinces
		- _tributum soli_ 
			- tribute of the soil
			- pay for being there
		- _tributum capitis_ 
			- tribute of the head
			- pay for population
		- Way it's done is described below
			- ruinous, overtaxes
			- economic activity slows
			- brilliant in that taxes are always paid


_Pontus Pilot_
Comes to Judea to be Roman governor.
Sees no symbols or standards above where they have legionary headquarters.
Says to mount shields up so people can find them.
Judea is a nationality and a religion.
Riot happens, people are throwing rocks at soldiers.
Pontus is told to take shields down and everything will be fine.
Riots stop then. He didn't have to quench rebellion with army.
Shields are decorated with images of pagan gods (Augustus).
Offense was taken because images of gods are not allowed in holy city.


Victory of Rome over Hannibal was because of first system.
With this system they had conquered all of Rome.
Then went to war with Carthage (and greek) city states on Scicily
because Rome's cities were threatened.
They won, with notable wins and losses.
Rome's war boats had tons of oars and bronze fronts for ramming.
**Not rowed by slaves!** By free sailors and soldiers, so they can board.
Also had bronze beaked bridge that was used to trap ships and board others.
Ships were unstable because of this, and storms took down some fleets, but they won most battles still.
Now Rome has cities outside of mainland Rome.
They need a new system to keep everyone safe. (see #2)
The ruler of a separate area needs imperium to command stationed troops.
Consuls can only rule for a year, so what if a war takes too long?
Then they are the _proconsul_ or _propraetor_, they aren't that person,
but they act _for/on behalf the conusl/praetor_.
They hold imperium; limited in time (as all imperium)
but also limited in space (only in local area).
Not elected, but appointed by senate to extend office after end of office.
They'll use this position to govern foreign lands.

_publicanus_ publican
NOT keeper of pub, or republican
Private contractor who does work for the Roman state
more specifically: **tax farmer**
Guy bids for job to collect taxes as a tax farmer.
Pays for taxes out of his own pocket.
Then he pays a guy to collect taxes for him.

- _publicanus I_: `collects taxes + profits for self`
	- bid to be able to collect taxes
- _national manager_: `collect taxes + profit I + profit II`
	- collects taxes from nation
	- assigned by I
- _city managers_: `collect taxes + profit I + II + III`
	- collect taxes from cities
	- assigned by national manager
- _neighborhood managers_: `taxes + I + II + III + IV`
	- collect taxes from aread of cities
	- assigned by city manager
- _street collector_: `taxes + I + II + III + IV + V`
	- actually go door to door to collect taxes
	- _nicodomus_ from the bible is one of these
	- tend to be hated
	- know the area and can get bribes if someone is trying to cheat taxes


### Roman Civilization

1. Confederation of city-states
2. polytheistic in religion
3. ruled from Rome

3 irrelevant during Roman Empire. The government is wherever the emperor is.

During Roman period of 4 emperors, Judea decided to rebel, but they were eventually crushed
after 3 years.
Rome won by conquering Jerusalem. And then completely destroying it.
Then, they started taxing if you were circumsized.

Rome's government also failed in germany.
`Thunar <=> Jupiter <=> Zeus (pater)`
This comparison could not be made with the Jewish religion.
Polytheism is not a problem in germany, unlike Judea,
the problem was the confederation of cities.
Rome easily won wars against germany many many times.
They couldn't win the peace though, because there was no urban life to assimilate.

